<?php
// require_once('database.php');
?>

<!DOCTYPE html>
<html>
<head>
	<title>SIGN UP</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
    <?php 
        include("path.php");
        include(ROOT_PATH . "/Views/header.php");
    ?>

    <div class='body-login'>

        <form action="Controllers/check_register.php" method="post">
     	    <h2>INSCRIVEZ-VOUS</h2>
     	    <?php if (isset($_GET['error'])) { ?>
     		    <p class="error"><?php echo $_GET['error']; ?></p>
     	    <?php } ?>

            <?php if (isset($_GET['success'])) { ?>
                <p class="success"><?php echo $_GET['success']; ?></p>
            <?php } ?>

            <?php if (isset($_GET['fname'])) { ?>
                <input type="text" 
                    name="fname" 
                    placeholder="Prénom"
                    value="<?php echo $_GET['fname']; ?>">
            <?php } else { ?>
                <input type="text" 
                    name="fname" 
                    placeholder="Prénom">
            <?php }?>
            
            <?php if (isset($_GET['lname'])) { ?>
                <input type="text" 
                    name="lname" 
                    placeholder="Nom"
                    value="<?php echo $_GET['lname']; ?>">
            <?php } else { ?>
                <input type="text" 
                    name="lname" 
                    placeholder="Nom">
            <?php }?>

            <?php if (isset($_GET['email'])) { ?>
                <input type="email" 
                    name="email" 
                    placeholder="Email"
                    value="<?php echo $_GET['email']; ?>">
            <?php } else { ?>
                <input type="text" 
                    name="email" 
                    placeholder="Email">
            <?php }?>

            <?php if (isset($_GET['uname'])) { ?>
                <input type="text" 
                    name="uname" 
                    placeholder="Pseudo"
                    value="<?php echo $_GET['uname']; ?>">
            <?php } else { ?>
                <input type="text" 
                    name="uname" 
                    placeholder="Pseudo">
            <?php }?>

     	    <input type="password" 
                name="pass" 
                placeholder="Mot de passe">

            <input type="password" 
                name="cpass" 
                placeholder="Confirmer le mot de passe"><br>

     	    <button type="submit">Devenir membre</button>
            <a href="index.php" class="new-account">Vous êtes déjà membre ?</a>
        </form>
    </div>
</body>
</html>