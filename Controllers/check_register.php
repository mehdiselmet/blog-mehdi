<?php

require_once('checkdata.php') ;
require_once('sendmail.php') ;
require_once('../database.php');

$errorMessage = "";

$code="";
    
$uname = validate($_POST['uname']) ;
$email = validate($_POST['email']) ;
$pass = validate($_POST['pass']) ;
$cpass = validate($_POST['cpass']) ;

$query = $pdo->prepare("SELECT count(*) FROM users WHERE username = ?") ;
$query->execute([$uname]); 
$count = $query->fetchColumn();

//Fonction PHP pour controler une adresse Mail
// function validateMail($email) {
//     return filter_var($email, FILTER_VALIDATE_EMAIL);
// }
 
if($count==0) {
    if ($pass != $cpass) {
        $errorMessage = "Erreur dans votre mot de passe ou votre Pseudo" ;
        header("Location: ../inscription.php?error=Pseudo ou mot de passe incorrect");
        exit;
    } else {
        $pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 12]);
        // $code = mt_rand(100000000000001, 999999999999999) ;
        $code = password_hash($uname . $email, PASSWORD_DEFAULT);
    
    /* préparation de la requête d'insertion */
    $query= $pdo->prepare("INSERT INTO users VALUES(NULL, :fname, :lname, :email, :uname, '$pass', NULL, '0', '$code', 5, '', NULL)");
 
    /* liaison de chaque champ de la table users à un input du formulaire */
    $query->bindValue(':fname', $_POST['fname'], PDO::PARAM_STR) ;
    $query->bindValue(':lname', $_POST['lname'], PDO::PARAM_STR) ;
    $query->bindValue(':email', $_POST['email'], PDO::PARAM_STR) ;
    $query->bindValue(':uname', $_POST['uname'], PDO::PARAM_STR) ;
    
    /* execution de la requête préparée*/
    $insertOk = $query->execute();
    }
    
    if($insertOk) {
        $object = "Demande de confirmation de votre inscription au blog" ;
        $content = "Veuillez confirmer votre email en cliquant sur le lien ci-contre : " ;
        $message = "<a href='http://localhost/index.php/projet-blog/Controllers/verifyemail.php?vcode=$code'>Register Account</a>";
        $content .= $message ;
        $res = smtpMailer("$email", "bla bla bla du blog", $content);
        header('Location: ../Views/thankyou.php') ;
    } else {
        echo "Echec d'insertion" ;
    }
} else {
    echo "Le Pseudo existe déjà" ;
    header("Location: ../inscription.php?error=Le pseudo utilisé existe déjà");
}
    
?>