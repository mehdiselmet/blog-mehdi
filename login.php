<?php 
    session_start();
?>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cité Blanche Gutenberg</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
</head>

<body>  
    <?php 
        include("path.php");
        include(ROOT_PATH . "/Views/header.php");
    ?>
    <div class='body-login'>
        <form action="Controllers/connexion.php" method="post">
     	    <h2 class='login-title'>CONNECTEZ-VOUS</h2>
     	    <?php if (isset($_GET['error'])) { ?>
     		    <p class="error"><?php echo $_GET['error']; ?></p>
     	    <?php } ?>
     	    
            <input type="text" name="uname" placeholder="Pseudo">

     	    <input type="password" name="pass" placeholder="Mot de passe"><br>
            
            <?php 
                // if (isset($_GET['error'])) { 
            ?>
                <!-- <input type="text" name="kode" placeholder="Veuillez entrer votre code d'activation"> -->
                <!-- <p class="error">  <?php 
                                            // echo $_GET['error']; 
                                        ?>
                                        </p> -->
     	    <?php 
                // } 
            ?>

     	    <button type="submit">Connectez-vous</button>
            <a href="inscription.php" class="new-account">Inscrivez-vous</a>
        </form>
    </div>
</body>
</html>

