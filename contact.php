<?php
    require_once('database.php');
    include("path.php");
    include(ROOT_PATH . "/Views/header.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/style.css">
    <title>Document</title>
</head>
<body>
    <div class='body-login'>
        <form action="">
            <h1>Contact</h1>
            <div class="separation"></div>
            <div class="corps-formulaire">
                <div class="boite">
                    <input type="text" placeholder="pseudo">
                    <textarea name="textarea" rows="10" cols="50" placeholder="Votre message"></textarea>
                </div>    
                <button type="submit">Envoyez votre message</button>
            </div>
        </form>
    </div>
</body>
</html>