 <!-- footer -->
 <!-- <link rel="stylesheet" href="./assets/css/stylefooter.css"> -->

 <div class="footer">
    <div class="footer-content">

      <div class="footer-section about">
        <h1 class="logo-text"><span>Cité Blanche Gutenberg</h1>
        <p>La Cité Blanche Gutenberg est un blog dédié aux anciens habitants des bidonvilles et des cités de transit.</p>
        <div class="contact">
          <span><i class="fas fa-envelope"></i> &nbsp; contact@cbg.com</span>
          <br>
        </div>
        <div class="socials">
          <a href="#"><i class="fab fa-facebook"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
          <a href="#"><i class="fab fa-twitter"></i></a>
          <a href="#"><i class="fab fa-youtube"></i></a>
        </div>
      </div>

      <div class="footer-section links">
        <h2>Quelques liens</h2>
        <ul>
          <a href="#">
            <li>Galerie Photos</li>
          </a>
          <a href="#">
            <li>Charte du blog</li>
          </a>
        </ul>
      </div>
    </div>

    <div class="footer-bottom">
      &copy; Blog réalisé par Mehdi
    </div>
  </div>
  <!-- // footer -->