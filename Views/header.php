<html>
<!-- <link rel="stylesheet" href="./assets/css/style.css"> -->
<header class="topmenu">
    <a href= "http://localhost/index.php/projet-blog/index.php" class="blog-logo">Cité Blanche Gutenberg</a>
    <nav class="menu-link">
        <a href="http://localhost/index.php/projet-blog/index.php" class="active">Accueil</a>
        <a href="#">Catégories</a>
        <a href="#">Archives</a>
        <a href="http://localhost/index.php/projet-blog/contact.php">Contact</a>
    </nav>

<!--     
    <nav class="login">
        <a href="http://localhost/Projets_MEHDI/projet-blog/inscription.php">S'inscrire</a>
        <a href="http://localhost/Projets_MEHDI/projet-blog/login.php">Se connecter</a>
    </nav>
-->

    <?php if (isset($_SESSION['username'])): ?>
        <nav class="login">
            <a href="#">
                <i class="fa fa-user"></i>
                <?php echo $_SESSION['username']; ?>
                <i class="fa fa-chevron-down" style="font-size: .8em;"></i>
            </a>
            <a href="<?php echo '/logout.php' ?>" class="logout">Déconnexion</a>
        </nav>
    <?php else: ?>
        <nav class="login">
            <a href="<?php echo 'http://localhost/index.php/projet-blog/inscription.php' ?>">S'inscrire</a>
            <a href="<?php echo 'http://localhost/index.php/projet-blog/login.php' ?>">Se connecter</a>
        </nav>
    <?php endif; ?>
</header>
</html> 